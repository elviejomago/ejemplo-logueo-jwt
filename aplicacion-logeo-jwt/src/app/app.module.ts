/* Componentes de angular*/
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";

/* Componentes de PRIMENG*/
import { ButtonModule } from "primeng/button";
import { LoginComponent } from "./componentes/login/login.component";
import { ToastModule } from "primeng/toast";
import { MessageService } from "primeng/api";
import { MessageModule } from "primeng/message";
import { PanelMenuModule } from "primeng/panelmenu";
import { ScrollPanelModule } from 'primeng/scrollpanel';
import {TableModule} from 'primeng/table';

/* Componentes de terceros*/

/* Componentes Desarrollados*/
import { AppComponent } from "./app.component";
import { InicioComponent } from "./componentes/inicio/inicio.component";

@NgModule({
  declarations: [AppComponent, LoginComponent, InicioComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule,
    BrowserAnimationsModule,
    MessageModule,
    HttpClientModule,
    PanelMenuModule,
    ScrollPanelModule,
    TableModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
