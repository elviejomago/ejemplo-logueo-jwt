import { CabeceraTo } from "../utilitario/cabecera-to";

export class RolTo{
  id: string;
  codigoEstado: string;
  nombre: string;
  cabeceraTo: CabeceraTo;
}
