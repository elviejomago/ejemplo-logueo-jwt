import { MenuItem } from 'primeng/api';

export class MenuItemImpl implements MenuItem {
    label: string;
    icon: string;
    url: string;
    routerLink: any;
    items: MenuItemImpl[];
    expanded: boolean;
    disabled: boolean;
    visible: boolean;
    target: string;
    routerLinkActiveOptions: any;
    separator: boolean;
    badge: string;
    badgeStyleClass: string;
    style: any;
    styleClass: string;
    title: string;
    id: string;
    automationId: any;
}
