export class MenuTo {
  id: string;
  codigoEstado: string;
  nombre:string;
  url:string;
  menuPadreTo:MenuTo;
  listaMenuHijosTo: MenuTo[];
}
