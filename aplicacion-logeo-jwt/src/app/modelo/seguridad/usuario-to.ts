import { CabeceraTo } from "../utilitario/cabecera-to";
import { RolTo } from './rol-to';
import { MenuTo } from './menu-to';

export class UsuarioTo{
  id: string;
  identificacion: string;
  codigoEstado: string;
  claseEstado: string;
  nombre: string;
  usuario: string;
  clave: string;
  token: string;
  listaRoles:RolTo[];
  listaMenusTo:MenuTo[];
  listaUsuariosTo:UsuarioTo[];
  cabeceraTo: CabeceraTo;
}
