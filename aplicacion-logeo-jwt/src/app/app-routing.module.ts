import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./componentes/login/login.component";
import { InicioComponent } from "./componentes/inicio/inicio.component";
import { RutaConstante } from "./constantes/ruta-constante";

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: RutaConstante.INICIO, component: InicioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
