import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { SeguridadService } from 'src/app/servicios/seguridad/seguridad.service';
import { MenuTo } from 'src/app/modelo/seguridad/menu-to';
import { MenuItemImpl } from 'src/app/modelo/seguridad/menu-item-impl';
import { ServicioWebConstante } from 'src/app/constantes/servicio-web-constante';
import { MensajeConstante } from 'src/app/constantes/mensaje-constante';
import { UsuarioTo } from 'src/app/modelo/seguridad/usuario-to';
import { Router } from '@angular/router';
import { RutaConstante } from 'src/app/constantes/ruta-constante';

@Component({
  selector: "app-inicio",
  templateUrl: "./inicio.component.html",
  styleUrls: ["./inicio.component.css"]
})
export class InicioComponent implements OnInit {
  listaMenuItem: MenuItemImpl[] = [];
  listaUsuarios: UsuarioTo[] = [];
  nombreUsuario:string;

  constructor(private seguridadService:SeguridadService, private router: Router, private messageService: MessageService) {}

  ngOnInit() {
    const listaMenus:MenuTo[] = this.seguridadService.usuarioLogueado.listaMenusTo;
    this.nombreUsuario = this.seguridadService.usuarioLogueado.nombre;
    for (let index = 0; index < listaMenus.length; index++) {
      const menuPadre = listaMenus[index];
      let menuItemPadre:MenuItemImpl = new MenuItemImpl();
      menuItemPadre.label = menuPadre.nombre;
      menuItemPadre.items = [];

      for (let index = 0; index < menuPadre.listaMenuHijosTo.length; index++) {
        const menuHijo = menuPadre.listaMenuHijosTo[index];
        let menuItemHijo:MenuItemImpl = new MenuItemImpl();
        menuItemHijo.label = menuHijo.nombre;
        menuItemHijo.icon = 'fa fa-minus-square';
        menuItemPadre.items.push(menuItemHijo);
      }

      this.listaMenuItem.push(menuItemPadre);
    }

    this.seguridadService.listarUsuarios(this.seguridadService.usuarioLogueado).subscribe(
      usuarioRespuesta => {
        if (usuarioRespuesta.cabeceraTo.codigo == ServicioWebConstante.CODIGO_SERVICIO_CORRECTO) {
          this.listaUsuarios = usuarioRespuesta.listaUsuariosTo;
        } else if (usuarioRespuesta.cabeceraTo.codigo == ServicioWebConstante.CODIGO_SERVICIO_ACCESO_DENEGADO) {
          this.messageService.add({ key: "mensaje", severity: "error", summary: "", detail: usuarioRespuesta.cabeceraTo.mensaje });
        } else {
          this.messageService.add({ key: "mensaje", severity: "error", summary: "", detail: MensajeConstante.MENSAJE_ERROR_ADMINISTRADOR });
          console.error("LoginComponent:ingresar: " + usuarioRespuesta.cabeceraTo.mensaje);
        }
      },
      error => {
        this.messageService.add({ key: "mensaje", severity: "error", summary: "", detail: error.message });
        console.error("LoginComponent:ingresar: " + error.message);
      }
    );
  }

  salir(){
    this.seguridadService.usuarioLogueado = null;
    this.router.navigateByUrl(RutaConstante.LOGIN);
  }
}
