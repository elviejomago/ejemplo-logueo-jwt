import { Component, OnInit } from "@angular/core";

import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { MessageService } from "primeng/api";
import { Router } from "@angular/router";
import { SeguridadService } from "src/app/servicios/seguridad/seguridad.service";
import { UsuarioTo } from 'src/app/modelo/seguridad/usuario-to';
import { ServicioWebConstante } from 'src/app/constantes/servicio-web-constante';
import { MensajeConstante } from 'src/app/constantes/mensaje-constante';
import { RutaConstante } from 'src/app/constantes/ruta-constante';
import { EstadoConstante } from 'src/app/constantes/estado-constante';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  frmLogin: FormGroup;

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private router: Router,
    private seguridadService: SeguridadService
  ) { }

  ngOnInit() {
    this.frmLogin = this.fb.group({
      'txtUsuario': new FormControl('', Validators.required),
      'txtClave': new FormControl('', Validators.required),
  });
  }

  ingresar() {
    let usuarioTo: UsuarioTo = new UsuarioTo();
    usuarioTo.usuario = this.frmLogin.get('txtUsuario').value;
    usuarioTo.clave = this.frmLogin.get('txtClave').value;
    usuarioTo.claseEstado = EstadoConstante.CLASE_EST_GENERAL;
    usuarioTo.codigoEstado = EstadoConstante.CODIGO_EST_GENERAL_ACTIVO;

    this.seguridadService.validarIngresoUsuario(usuarioTo).subscribe(
      usuarioTo => {
        if (usuarioTo.cabeceraTo.codigo == ServicioWebConstante.CODIGO_SERVICIO_CORRECTO) {
          this.seguridadService.usuarioLogueado = usuarioTo;
          this.router.navigateByUrl(RutaConstante.INICIO);
        } else if (usuarioTo.cabeceraTo.codigo == ServicioWebConstante.CODIGO_SERVICIO_ACCESO_DENEGADO) {
          this.messageService.add({ key: "mensaje", severity: "error", summary: "", detail: usuarioTo.cabeceraTo.mensaje });
        } else {
          this.messageService.add({ key: "mensaje", severity: "error", summary: "", detail: MensajeConstante.MENSAJE_ERROR_ADMINISTRADOR });
          console.error("LoginComponent:ingresar: " + usuarioTo.cabeceraTo.mensaje);
        }

        this.frmLogin.get('txtUsuario').setValue(null);
        this.frmLogin.get('txtClave').setValue(null);
      },
      error => {
        this.frmLogin.get('txtUsuario').setValue(null);
        this.frmLogin.get('txtClave').setValue(null);
        this.messageService.add({ key: "mensaje", severity: "error", summary: "", detail: error.message });
        console.error("LoginComponent:ingresar: " + error.message);
      }
    );
  }
}
