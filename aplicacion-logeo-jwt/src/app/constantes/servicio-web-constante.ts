export class ServicioWebConstante{
    public static readonly CODIGO_SERVICIO_ERROR:string = '0';
    public static readonly CODIGO_SERVICIO_CORRECTO:string = "1";
	public static readonly CODIGO_SERVICIO_ACCESO_DENEGADO:string = "2";
	public static readonly CODIGO_SERVICIO_INFORMACION_NO_EXISTE:string = "3";

    public static readonly MENSAJE_SERVICIO_ERROR:string = "Se ha producido un error en el servicio.";
	public static readonly MENSAJE_SERVICIO_CORRECTO:string = "Correcto.";
	public static readonly MENSAJE_SERVICIO_ACCESO_DENEGADO:string = "Acceso Denegado.";
	public static readonly MENSAJE_SERVICIO_INFORMACION_NO_EXISTE:string = "No existe información.";
}