export class EstadoConstante{

    public static readonly CLASE_EST_GENERAL = 'GENERAL';

    public static readonly CODIGO_EST_GENERAL_ACTIVO = 'ACT';
    public static readonly CODIGO_EST_GENERAL_INACTIVO = 'INACT';
}