export class MensajeConstante{
    public static readonly MENSAJE_ERROR_ADMINISTRADOR:string = 'Se ha producido un error en el sistema, comuniquese con el administrador.';
}