
import { Component, OnInit } from '@angular/core';
import { SeguridadService } from './servicios/seguridad/seguridad.service';
import { Router } from '@angular/router';
import { RutaConstante } from './constantes/ruta-constante';

/**
* @author Vinicio Ochoa
* @version 1.0
* 28/01/2019
* Clase para gestionar las cargadas iniciales.
*/
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(private seguridadService:SeguridadService, private router: Router) {

    }

    ngOnInit() {
        if(this.seguridadService.usuarioLogueado == null){
            this.router.navigateByUrl(RutaConstante.LOGIN);
        }
    }
}
