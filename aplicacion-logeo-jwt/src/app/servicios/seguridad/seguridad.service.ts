import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsuarioTo } from 'src/app/modelo/seguridad/usuario-to';

@Injectable({
  providedIn: 'root'
})
export class SeguridadService {

  public usuarioLogueado: UsuarioTo;

  URL_SERVICIO_SEGURIDAD_VALIDA_USUARIO = "http://localhost:8080/servicio-ws-seguridad/recurso-web/seguridad/publico/validar/usuario";
  URL_SERVICIO_SEGURIDAD_LISTA_USUARIO = "http://localhost:8080/servicio-ws-seguridad/recurso-web/seguridad/lista/usuarios";

  constructor(private httpClient:HttpClient) { }

  validarIngresoUsuario(usuarioTo:UsuarioTo): Observable<UsuarioTo> {
    let options = {
      reportProgress: true
    };

    return this.httpClient.post<UsuarioTo>(this.URL_SERVICIO_SEGURIDAD_VALIDA_USUARIO, usuarioTo, options);
  }

  listarUsuarios(usuarioTo:UsuarioTo): Observable<UsuarioTo> {
    let httpHeaders = new HttpHeaders().set('Authorization', usuarioTo.token);

    let options = {
      headers: httpHeaders,
      reportProgress: true
    };

    return this.httpClient.post<UsuarioTo>(this.URL_SERVICIO_SEGURIDAD_LISTA_USUARIO, null, options);
  }

}
