package ec.servicio.seguridad.modelo.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MenuTo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 173909093650653727L;

	private String id;
	private String codigoEstado;
	private String nombre;
	private String url;
	private MenuTo menuPadreTo;
	private List<MenuTo> listaMenuHijosTo;
	
	private CabeceraTo cabeceraTo;
	
	public MenuTo() {
		super();
		this.listaMenuHijosTo = new ArrayList<MenuTo>();
	}
}
