package ec.servicio.seguridad.modelo.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioTo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 173919393650953727L;

	private String id;
	private String identificacion;
	private String codigoEstado;
	private String claseEstado;
	private String nombre;
	private String usuario;
	private String clave;
	private String token;
	private List<RolTo> listaRolesTo;
	private List<MenuTo> listaMenusTo;
	private List<UsuarioTo> listaUsuariosTo;
	private CabeceraTo cabeceraTo;
	
	public UsuarioTo() {
		super();
		this.listaRolesTo = new ArrayList<RolTo>();
		this.listaMenusTo = new ArrayList<MenuTo>();
		this.listaUsuariosTo = new ArrayList<UsuarioTo>();
	}
}
