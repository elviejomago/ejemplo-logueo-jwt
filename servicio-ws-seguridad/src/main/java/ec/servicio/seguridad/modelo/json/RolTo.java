package ec.servicio.seguridad.modelo.json;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RolTo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 173919393650053727L;

	private String id;
	private String codigoEstado;
	private String nombre;
	
	private CabeceraTo cabeceraTo;
	
	public RolTo() {
		super();
	}
}
