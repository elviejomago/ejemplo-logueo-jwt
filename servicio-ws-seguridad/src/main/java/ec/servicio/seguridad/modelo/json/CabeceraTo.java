package ec.servicio.seguridad.modelo.json;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabeceraTo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 173919313650953027L;

	private String codigo;
	private String mensaje;
	
	public CabeceraTo() {
		super();
	}
}
