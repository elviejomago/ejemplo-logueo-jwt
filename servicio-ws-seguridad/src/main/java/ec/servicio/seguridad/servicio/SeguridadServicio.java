package ec.servicio.seguridad.servicio;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ec.servicio.seguridad.dao.SeguridadDao;
import ec.servicio.seguridad.modelo.json.MenuTo;
import ec.servicio.seguridad.modelo.json.UsuarioTo;
import ec.servicio.seguridad.utilitario.LogUtilitario;
import ec.servicio.seguridad.utilitario.ServicioUtilitario;
import ec.servicio.seguridad.utilitario.TokenUtilitario;

@Singleton
@Consumes(MediaType.APPLICATION_JSON)
@Path("seguridad")
@Lock(LockType.READ)
public class SeguridadServicio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8167402541725383086L;

	@EJB
	private SeguridadDao seguridadDao;

	@POST
	@Path("publico/validar/usuario")
	@Produces(MediaType.APPLICATION_JSON)
	public UsuarioTo validarUsuario(UsuarioTo usuarioTo) {
		try {
			usuarioTo = seguridadDao.validarUsuario(usuarioTo);
			if (usuarioTo.getId() == null) {
				usuarioTo.setToken(null);
				usuarioTo.setCabeceraTo(ServicioUtilitario.generarCabeceraAccesoDenegado());
				return usuarioTo;
			} else {
				usuarioTo.setToken(TokenUtilitario.generarToken(Long.parseLong(usuarioTo.getId())));
				if (usuarioTo.getListaRolesTo() != null && !usuarioTo.getListaRolesTo().isEmpty()) {
					List<MenuTo> listaMenuPadre = seguridadDao.obtenerMenusPadre(usuarioTo.getListaRolesTo());

					for (MenuTo menu : listaMenuPadre) {
						MenuTo menuPadreTo = new MenuTo();
						menuPadreTo.setId(menu.getId().toString());
						menuPadreTo.setNombre(menu.getNombre());
						menuPadreTo.setUrl(menu.getUrl());

						List<MenuTo> listaMenuHijo = seguridadDao.obtenerMenusPorPadreId(menu.getId(), usuarioTo.getListaRolesTo());
						for (MenuTo menuHijo : listaMenuHijo) {
							MenuTo menuHijoTo = new MenuTo();
							menuHijoTo.setId(menuHijo.getId().toString());
							menuHijoTo.setNombre(menuHijo.getNombre());
							menuHijoTo.setUrl(menuHijo.getUrl());
							menuPadreTo.getListaMenuHijosTo().add(menuHijoTo);
						}

						usuarioTo.getListaMenusTo().add(menuPadreTo);
					}

				}

				usuarioTo.setCabeceraTo(ServicioUtilitario.generarCabeceraCorrecto());
			}
		} catch (RuntimeException e) {
			LogUtilitario.log(SeguridadServicio.class).error("validarUsuario", e);
			usuarioTo.setCabeceraTo(ServicioUtilitario.generarCabeceraError());
		}
		return usuarioTo;
	}
	
	@POST
	@Path("lista/usuarios")
	@Produces(MediaType.APPLICATION_JSON)
	public UsuarioTo listarUsuarios() {
		UsuarioTo usuarioTo = new UsuarioTo();
		try {
			for (int i = 0; i < 10; i++) {
				UsuarioTo usuarioTemporal = new UsuarioTo();
				usuarioTemporal.setIdentificacion("123456879"+i);
				usuarioTemporal.setNombre("Usuario "+i);
				if(i % 2 == 0) {
					usuarioTemporal.setCodigoEstado("ACTIVO");
				}else {
					usuarioTemporal.setCodigoEstado("INACTIVO");
				}
				
				usuarioTo.getListaUsuariosTo().add(usuarioTemporal);
			}
			
			usuarioTo.setCabeceraTo(ServicioUtilitario.generarCabeceraCorrecto());
		} catch (RuntimeException e) {
			LogUtilitario.log(SeguridadServicio.class).error("validarUsuario", e);
			usuarioTo.setCabeceraTo(ServicioUtilitario.generarCabeceraError());
		}
		return usuarioTo;
	}
}
