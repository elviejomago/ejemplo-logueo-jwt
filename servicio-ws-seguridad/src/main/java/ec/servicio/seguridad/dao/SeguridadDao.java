package ec.servicio.seguridad.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import ec.servicio.seguridad.modelo.json.MenuTo;
import ec.servicio.seguridad.modelo.json.RolTo;
import ec.servicio.seguridad.modelo.json.UsuarioTo;

@Stateless
public class SeguridadDao {

	public UsuarioTo validarUsuario(UsuarioTo usuarioTo) throws RuntimeException {
		if (usuarioTo.getUsuario().equals("admin") && usuarioTo.getClave().equals("admin")) {
			usuarioTo.setId("1234567890");
			usuarioTo.setNombre("Administrador");
			usuarioTo.setListaRolesTo(listarRolUsuario(usuarioTo));
		} 
		return usuarioTo;
	}

	public List<RolTo> listarRolUsuario(UsuarioTo usuarioTo) throws RuntimeException {
		RolTo rolTo = new RolTo();
		rolTo.setId("1");
		rolTo.setNombre("Rol Administrador");

		List<RolTo> listaRoles = new ArrayList<RolTo>();
		listaRoles.add(rolTo);
		return listaRoles;
	}

	public List<MenuTo> obtenerMenusPadre(List<RolTo> roles) throws RuntimeException {
		MenuTo menuTo = new MenuTo();
		menuTo.setId("1");
		menuTo.setNombre("Menu Padre 1");
		menuTo.setUrl("#");

		MenuTo menuTo2 = new MenuTo();
		menuTo2.setId("2");
		menuTo2.setNombre("Menu Padre 2");
		menuTo2.setUrl("#");

		List<MenuTo> listaMenus = new ArrayList<MenuTo>();
		listaMenus.add(menuTo);
		listaMenus.add(menuTo2);

		return listaMenus;
	}

	public List<MenuTo> obtenerMenusPorPadreId(String padreId, List<RolTo> roles) throws RuntimeException {
		MenuTo menuTo = new MenuTo();
		menuTo.setId("1.1");
		menuTo.setNombre("Menu Hijo 1");
		menuTo.setUrl("/url1");

		MenuTo menuTo2 = new MenuTo();
		menuTo2.setId("1.1");
		menuTo2.setNombre("Menu Hijo 2");
		menuTo2.setUrl("/url2");

		List<MenuTo> listaMenus = new ArrayList<MenuTo>();
		listaMenus.add(menuTo);
		listaMenus.add(menuTo2);

		return listaMenus;
	}

}
