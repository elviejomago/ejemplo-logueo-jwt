package ec.servicio.seguridad.utilitario;

import ec.servicio.seguridad.constante.ServicioWebConstante;
import ec.servicio.seguridad.modelo.json.CabeceraTo;

public class ServicioUtilitario {

	public static CabeceraTo generarCabeceraCorrecto() {
		CabeceraTo cabeceraTo = new CabeceraTo();
		cabeceraTo.setCodigo(ServicioWebConstante.CODIGO_SERVICIO_CORRECTO);
		cabeceraTo.setMensaje(ServicioWebConstante.MENSAJE_SERVICIO_CORRECTO);
		return cabeceraTo;
	}

	public static CabeceraTo generarCabeceraError() {
		CabeceraTo cabeceraTo = new CabeceraTo();
		cabeceraTo.setCodigo(ServicioWebConstante.CODIGO_SERVICIO_ERROR);
		cabeceraTo.setMensaje(ServicioWebConstante.MENSAJE_SERVICIO_ERROR);
		return cabeceraTo;
	}

	public static CabeceraTo generarCabeceraError(String error) {
		CabeceraTo cabeceraTo = new CabeceraTo();
		cabeceraTo.setCodigo(ServicioWebConstante.CODIGO_SERVICIO_ERROR);
		cabeceraTo.setMensaje(error);
		return cabeceraTo;
	}

	public static CabeceraTo generarCabeceraInformacionNoExiste() {
		CabeceraTo cabeceraTo = new CabeceraTo();
		cabeceraTo.setCodigo(ServicioWebConstante.CODIGO_SERVICIO_INFORMACION_NO_EXISTE);
		cabeceraTo.setMensaje(ServicioWebConstante.MENSAJE_SERVICIO_INFORMACION_NO_EXISTE);
		return cabeceraTo;
	}

	public static CabeceraTo generarCabeceraAccesoDenegado() {
		CabeceraTo cabeceraTo = new CabeceraTo();
		cabeceraTo.setCodigo(ServicioWebConstante.CODIGO_SERVICIO_ACCESO_DENEGADO);
		cabeceraTo.setMensaje(ServicioWebConstante.MENSAJE_SERVICIO_ACCESO_DENEGADO);
		return cabeceraTo;
	}

}
