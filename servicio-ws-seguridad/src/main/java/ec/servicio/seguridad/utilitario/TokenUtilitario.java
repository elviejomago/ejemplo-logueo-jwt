package ec.servicio.seguridad.utilitario;

import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

import ec.servicio.seguridad.constante.SeguridadConstante;

public class TokenUtilitario {

	public static String generarToken(Long idUsuario) {
        Calendar calendario = Calendar.getInstance();
        calendario.add(Calendar.HOUR, Integer.parseInt(SeguridadConstante.TIEMPO_CADUCIDAD_TOKEN_HORAS));
        Algorithm algorithm = Algorithm.HMAC512(SeguridadConstante.CLAVE_SECRETA_PRIVADA_JWT);

        String token = JWT.create().withIssuer(SeguridadConstante.METODO_AUTENTICACION_JWT)
                .withClaim("idUsuario", idUsuario).withIssuedAt(new Date()).withExpiresAt(calendario.getTime())
                .sign(algorithm);
        
        return Base64.getEncoder().encodeToString(token.getBytes());
    }

	public static boolean validarToken(Map<String, String> infoCabeceras) {
		String token = null;
        if (infoCabeceras.containsKey("Authorization")) {
            token = infoCabeceras.get("Authorization");
        } else {
            token = infoCabeceras.get("authorization");
        }

        return validarToken(token);
	}
	
	public static boolean validarToken(String token) {
        boolean validarToken = true;
        try {
            Algorithm algorithm = Algorithm.HMAC512(SeguridadConstante.CLAVE_SECRETA_PRIVADA_JWT);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer(SeguridadConstante.METODO_AUTENTICACION_JWT).build();
            verifier.verify(new String(Base64.getDecoder().decode(token)));
        } catch (RuntimeException e) {
        	Logger.getLogger(TokenUtilitario.class).error("validarToken", e);
        	validarToken = false;
        }
        return validarToken;
    }

}
