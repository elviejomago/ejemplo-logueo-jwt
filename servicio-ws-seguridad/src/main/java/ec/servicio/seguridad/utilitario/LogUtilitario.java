package ec.servicio.seguridad.utilitario;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class LogUtilitario {
	
	public static Logger log(Class<?> clase) {
		BasicConfigurator.configure();
		return Logger.getLogger(clase);
	}
}
