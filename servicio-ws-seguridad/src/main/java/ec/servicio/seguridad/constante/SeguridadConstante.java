package ec.servicio.seguridad.constante;

public class SeguridadConstante {

	public static final String METODO_AUTENTICACION_JWT = "auth0";
	public static final String CLAVE_SECRETA_PRIVADA_JWT = "aplicaciondeprueba";
	public static final String TIEMPO_CADUCIDAD_TOKEN_HORAS = "4";
}
