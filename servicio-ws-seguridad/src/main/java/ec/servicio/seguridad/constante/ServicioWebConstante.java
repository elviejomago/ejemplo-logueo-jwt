package ec.servicio.seguridad.constante;

public class ServicioWebConstante{

	public static final String CODIGO_SERVICIO_ERROR = "0";
	public static final String CODIGO_SERVICIO_CORRECTO = "1";
	public static final String CODIGO_SERVICIO_ACCESO_DENEGADO = "2";
	public static final String CODIGO_SERVICIO_INFORMACION_NO_EXISTE = "3";

	public static final String MENSAJE_SERVICIO_ERROR = "Se ha producido un error en el servicio.";
	public static final String MENSAJE_SERVICIO_CORRECTO = "Correcto.";
	public static final String MENSAJE_SERVICIO_ACCESO_DENEGADO = "Acceso Denegado.";
	public static final String MENSAJE_SERVICIO_INFORMACION_NO_EXISTE = "No existe información.";

}
